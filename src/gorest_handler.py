import requests


class GoRESTHandler:
    base_url = "https://gorest.co.in/public/v2"
    users_endpoint = "/users"

    headers = {
        "Authorization": "Bearer 7327c85e10547e9df37b81ec701bf54f14c7e99d2e13acc78690c1da66330c81"
    }

    def create_user(self, user_data, expected_status_code=201):
        response = requests.post(self.base_url + self.users_endpoint, json=user_data, headers=self.headers)
        assert response.status_code == expected_status_code
        return response

    def get_user(self, user_id, expected_status_code=200):
        response = requests.get(f"{self.base_url}{self.users_endpoint}/{user_id}", headers=self.headers)
        assert response.status_code == expected_status_code
        return response

    def update_user(self, user_id, updated_user_data, expected_status_code=200):
        response = requests.put(f"{self.base_url}{self.users_endpoint}/{user_id}", json=updated_user_data, headers=self.headers)
        assert response.status_code == expected_status_code
        return response

    def delete_user(self, user_id, expected_status_code=204):
        response = requests.delete(f"{self.base_url}{self.users_endpoint}/{user_id}", headers=self.headers)
        assert response.status_code == expected_status_code
        return response
