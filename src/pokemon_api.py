import requests

def test_if_JSON_is_not_empty():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    response_body = response.json()

    assert response_body["results"]


def test_status_code_200():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")

    assert response.status_code == 200

def test_lenght_list_is_1279():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    response.body = response.json()
    assert response.body["count"] == 1279

