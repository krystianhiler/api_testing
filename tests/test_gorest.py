from src.gorest_handler import GoRESTHandler
from faker import Faker

gorest_handler = GoRESTHandler()


def test_create_user():
    user_data = {
        "name": "Tenali Ramakrishna",
        "gender": "male",
        "email": Faker().email(),
        "status": "active"
    }
    body = gorest_handler.create_user(user_data).json()
    assert "id" in body
    user_id = body["id"]
    body = gorest_handler.get_user(user_id).json()
    assert body["email"] == user_data["email"]
    assert body["name"] == user_data["name"]


def test_update_user():
    user_data = {
        "name": "Tenali Ramakrishna",
        "gender": "male",
        "email": Faker().email(),
        "status": "active"
    }
    body = gorest_handler.create_user(user_data).json()
    user_id = body["id"]
    updated_user_data = {
        "email": Faker().email()
    }
    gorest_handler.update_user(user_id, updated_user_data)
    body = gorest_handler.get_user(user_id).json()
    assert body["email"] == updated_user_data["email"]


def test_delete_user():
    user_data = {
        "name": "Tenali Ramakrishna",
        "gender": "male",
        "email": Faker().email(),
        "status": "active"
    }
    body = gorest_handler.create_user(user_data).json()
    user_id = body["id"]
    response = gorest_handler.delete_user(user_id)
    status_code = response.status_code
    assert status_code == 204



